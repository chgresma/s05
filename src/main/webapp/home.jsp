<!-- This jsp file serves as a greeting page to the user using the full name dynamically and render a different message if the user type is an employer or an applicant. -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Home Page</title>
	</head>
	<body>
		<!-- This displays the Concatenation of the First Name and the Last Name -->
		<h1>Welcome <%=session.getAttribute("fullName") %>! </h1>
		
		<!-- This displays the message depending on what is the userType -->
		<%
			//This fetches what userType is the user
			String userType = session.getAttribute("type").toString();
			
			//This indicates the condition for what message that will be prompted
			if(userType.equals("Applicant")){
				out.println("Welcome applicant. You may now start looking for your career opportunity.");
			}
			else{
				out.println("Welcome employer. You may start browsing applicant profiles.");
			}
		%>
		
	</body>
</html>