//This java file stores all of the data inputs in the HttpSession.
package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;
	
	
	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res)throws IOException{
		
		String first_name = req.getParameter("first_name");
		String last_name = req.getParameter("last_name");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String discovery = req.getParameter("discovery");
		String birthdate = req.getParameter("birthDate");
		String type = req.getParameter("type");
		String description = req.getParameter("description");
		
		//This stores all the data from the form to the session
		
		HttpSession session = req.getSession();
		
		session.setAttribute("first_name",first_name);
		session.setAttribute("last_name",last_name);
		session.setAttribute("phone",phone);
		session.setAttribute("email",email);
		session.setAttribute("discovery",discovery);
		session.setAttribute("birthDate",birthdate);
		session.setAttribute("type",type);
		session.setAttribute("description",description);
		
		
		//This sends the information to the register.jsp
		res.sendRedirect("register.jsp");
		
		
	}
	
	public void destroy(){
		System.out.println("RegisterServlet has been destroyed.");
	}
}