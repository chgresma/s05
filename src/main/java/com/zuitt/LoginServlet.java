//This java file will store the first and last name of the user and set a new HttpSession attribute for the fullname.
package com.zuitt;

import java.io.IOException;


import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8286177122798244808L;
	
	public void init()throws ServletException{
		System.out.println("LoginServlet has been initialized.");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res)throws IOException,ServletException{
		
		HttpSession session = req.getSession();
		String fname =session.getAttribute("first_name").toString();
		String lname = session.getAttribute("last_name").toString();
		String fullName=fname+" "+lname;
		
		
		//Store the first Name and the Last Name in the session
		
		session.setAttribute("fullName",fullName );
		
		
		//This sends the information to the home.jsp
		res.sendRedirect("home.jsp");
	}
	public void destroy(){
		System.out.println("LoginServlet has been destroyed.");
	}

}